import os
from socketserver import BaseRequestHandler, TCPServer
from threading import Thread
from multiprocessing import Queue

base_path = "/home/angelo/Documents/Universita/CompressioneDati/progetto/compressione/"
clients = Queue(3)


class ServerHandler(BaseRequestHandler):
    print("start")

    def handle(self):
        print("Got connection from ", self.client_address)

        clients.put(self.request)

        file = files.get()
        name = file.split('/')[-1]
        self.request.send(name.encode())

        size = os.stat(base_path + name).st_size

        with open(base_path + name, "rb") as fin:

            while not clients.full():
                print("attendo i client")

            """
            Funziona
            l = fin.read(10024)
            while l:
                self.request.send(l)
                l = fin.read(10024)
            """
            self.request.send(str(size).encode())   # invio la dimensione del file
            self.request.sendfile(fin)




if __name__ == '__main__':
    print("start main")
    base_path = input('Inserire la path alla directory')
    
    files = Queue()

    for f in os.listdir(base_path):
        files.put(base_path + f)

    serv = TCPServer(('', 20000), ServerHandler)

    for n in range(2):
        t = Thread(target=serv.serve_forever)
        t.daemon = True
        t.start()

    serv.serve_forever()
